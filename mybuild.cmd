set EIGEN_SRC=%CD%
set EIGEN_BUILD=%CD%/build
set DST=D:\DevNow\eigen

cd %EIGEN_BUILD%
cmake %EIGEN_SRC%
make install -DCMAKE_INSTALL_PREFIX=%DST%